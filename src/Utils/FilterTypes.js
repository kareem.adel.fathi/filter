export default FilterTypes = {
  HORIZONTAL_BUTTON_RADIO: 'horizontalBottonRadio',
  MIN_MAX_SEEK: 'minMaxSeek',
  SEEK: 'seek',
  GROUP_CHECKBOX: 'groupCheckBox',
  HORIZONTAL_BUTTON_CHECKBOX: 'horizontalButtonCheckbox',
  CHECKBOX: 'checkbox',
};

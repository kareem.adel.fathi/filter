import FilterTypes from './FilterTypes';

export const mockItemsWithFilters = {
  filters: [
    {
      type: FilterTypes.HORIZONTAL_BUTTON_RADIO,
      title: 'Buying Format',
      key: 'buyingFormat',
      values: [
        {title: 'Auction', key: 'auction'},
        {title: 'Direct Sale', key: 'directSale'},
      ],
    },
    {
      type: FilterTypes.HORIZONTAL_BUTTON_RADIO,
      title: 'Condition',
      key: 'condition',
      values: [
        {title: 'New', key: 'new'},
        {title: 'Used', key: 'used'},
      ],
    },
    {
      type: FilterTypes.GROUP_CHECKBOX,
      title: 'Category',
      key: 'category',
      values: [
        {title: 'Smart Phones', key: 'smartPhones'},
        {title: 'Laptops', key: 'laptops'},
      ],
    },
    {
      type: FilterTypes.MIN_MAX_SEEK,
      title: 'Price',
      key: 'price',
      min: 0,
      max: 5000,
    },
    {
      type: FilterTypes.MIN_MAX_SEEK,
      title: 'Quantity',
      key: 'quantity',
      min: 0,
      max: 90,
    },
    {
      type: FilterTypes.SEEK,
      title: 'Seller rate',
      key: 'sellerRate',
      min: 0,
      max: 5,
    },
    {
      type: FilterTypes.GROUP_CHECKBOX,
      title: 'Brand',
      key: 'brand',
      values: [
        {title: 'ACER', key: 'acer', quantity: 28},
        {title: 'ALCATEL', key: 'alcatel', quantity: 20},
        {title: 'ALLVIEW', key: 'allview', quantity: 17},
        {title: 'AMAZON', key: 'amazon', quantity: 8},
        {title: 'AMOI', key: 'amoi', quantity: 2},
        {title: 'APPLE', key: 'apple', quantity: 2},
        {title: 'ARCHOS', key: 'archos', quantity: 3},
        {title: 'ASUS', key: 'asus', quantity: 1},
        {title: 'AT & T', key: 'at & t', quantity: 20},
        {title: 'BENEFON', key: 'benefon', quantity: 6},
        {title: 'BENQ', key: 'benq', quantity: 24},
        {title: 'BENQ-SIEMENS', key: 'benq-siemens', quantity: 9},
        {title: 'BIRD', key: 'bird', quantity: 19},
        {title: 'BLACKBERRY', key: 'blackberry', quantity: 20},
        {title: 'BLACKVIEW', key: 'blackview', quantity: 14},
        {title: 'BLU', key: 'blu', quantity: 19},
        {title: 'BOSCH', key: 'bosch', quantity: 19},
        {title: 'BQ', key: 'bq', quantity: 5},
        {title: 'CASIO', key: 'casio', quantity: 5},
        {title: 'CAT', key: 'cat', quantity: 5},
        {title: 'CELKON', key: 'celkon', quantity: 7},
        {title: 'CHEA', key: 'chea', quantity: 10},
        {title: 'COOLPAD', key: 'coolpad', quantity: 26},
        {title: 'DELL', key: 'dell', quantity: 8},
        {title: 'EMPORIA', key: 'emporia', quantity: 5},
        {title: 'ENERGIZER', key: 'energizer', quantity: 2},
        {title: 'ERICSSON', key: 'ericsson', quantity: 11},
        {title: 'ETEN', key: 'eten', quantity: 6},
        {title: 'FAIRPHONE', key: 'fairphone', quantity: 0},
        {title: 'FUJITSU', key: 'fujitsu', quantity: 29},
        {title: 'SIEMENS', key: 'siemens', quantity: 20},
        {title: 'GARMIN-ASUS', key: 'garmin-asus', quantity: 0},
        {title: 'GIGABYTE', key: 'gigabyte', quantity: 17},
        {title: 'GIONEE', key: 'gionee', quantity: 2},
        {title: 'GOOGLE', key: 'google', quantity: 6},
        {title: 'HAIER', key: 'haier', quantity: 17},
        {title: 'HONOR', key: 'honor', quantity: 21},
        {title: 'HP', key: 'hp', quantity: 4},
        {title: 'HTC', key: 'htc', quantity: 14},
        {title: 'HUAWEI', key: 'huawei', quantity: 26},
        {title: 'I-MATE', key: 'i-mate', quantity: 29},
        {title: 'I-MOBILE', key: 'i-mobile', quantity: 7},
        {title: 'ICEMOBILE', key: 'icemobile', quantity: 23},
        {title: 'INFINIX', key: 'infinix', quantity: 29},
        {title: 'INNOSTREAM', key: 'innostream', quantity: 15},
        {title: 'INQ', key: 'inq', quantity: 16},
        {title: 'INTEX', key: 'intex', quantity: 10},
        {title: 'JOLLA', key: 'jolla', quantity: 19},
        {title: 'KARBONN', key: 'karbonn', quantity: 15},
        {title: 'KYOCERA', key: 'kyocera', quantity: 22},
        {title: 'LAVA', key: 'lava', quantity: 22},
        {title: 'LEECO', key: 'leeco', quantity: 27},
        {title: 'LENOVO', key: 'lenovo', quantity: 1},
        {title: 'LG', key: 'lg', quantity: 17},
        {title: 'MAXON', key: 'maxon', quantity: 8},
        {title: 'MAXWEST', key: 'maxwest', quantity: 27},
        {title: 'MEIZU', key: 'meizu', quantity: 1},
        {title: 'MICROMAX', key: 'micromax', quantity: 16},
        {title: 'MICROSOFT', key: 'microsoft', quantity: 16},
        {title: 'MITAC', key: 'mitac', quantity: 13},
        {title: 'MITSUBISHI', key: 'mitsubishi', quantity: 0},
        {title: 'MODU', key: 'modu', quantity: 28},
        {title: 'MOTOROLA', key: 'motorola', quantity: 6},
        {title: 'MWG', key: 'mwg', quantity: 19},
        {title: 'NEC', key: 'nec', quantity: 12},
        {title: 'NEONODE', key: 'neonode', quantity: 5},
        {title: 'NIU', key: 'niu', quantity: 9},
        {title: 'NOKIA', key: 'nokia', quantity: 10},
        {title: 'NVIDIA', key: 'nvidia', quantity: 15},
        {title: 'O2', key: 'o2', quantity: 27},
        {title: 'ONEPLUS', key: 'oneplus', quantity: 28},
        {title: 'OPPO', key: 'oppo', quantity: 27},
        {title: 'ORANGE', key: 'orange', quantity: 12},
        {title: 'PALM', key: 'palm', quantity: 15},
        {title: 'PANASONIC', key: 'panasonic', quantity: 12},
        {title: 'PANTECH', key: 'pantech', quantity: 12},
        {title: 'PARLA', key: 'parla', quantity: 27},
        {title: 'PHILIPS', key: 'philips', quantity: 10},
        {title: 'PLUM', key: 'plum', quantity: 5},
        {title: 'POSH', key: 'posh', quantity: 24},
        {title: 'PRESTIGIO', key: 'prestigio', quantity: 3},
        {title: 'QMOBILE', key: 'qmobile', quantity: 0},
        {title: 'QTEK', key: 'qtek', quantity: 12},
        {title: 'RAZER', key: 'razer', quantity: 6},
        {title: 'REALME', key: 'realme', quantity: 28},
        {title: 'SAGEM', key: 'sagem', quantity: 9},
        {title: 'SAMSUNG', key: 'samsung', quantity: 6},
        {title: 'SENDO', key: 'sendo', quantity: 11},
        {title: 'SEWON', key: 'sewon', quantity: 2},
        {title: 'SHARP', key: 'sharp', quantity: 14},
        {title: 'SIEMENS', key: 'siemens', quantity: 16},
        {title: 'SONIM', key: 'sonim', quantity: 19},
        {title: 'SONY', key: 'sony', quantity: 5},
        {title: 'SONY', key: 'sony', quantity: 10},
        {title: 'ERICSSON', key: 'ericsson', quantity: 6},
        {title: 'SPICE', key: 'spice', quantity: 18},
        {title: 'T-MOBILE', key: 't-mobile', quantity: 28},
        {title: 'TCL', key: 'tcl', quantity: 2},
        {title: 'TECNO', key: 'tecno', quantity: 17},
        {title: 'TEL.ME.', key: 'tel.me.', quantity: 5},
        {title: 'TELIT', key: 'telit', quantity: 9},
        {title: 'THURAYA', key: 'thuraya', quantity: 19},
        {title: 'TOSHIBA', key: 'toshiba', quantity: 16},
        {title: 'ULEFONE', key: 'ulefone', quantity: 17},
        {title: 'UNNECTO', key: 'unnecto', quantity: 22},
        {title: 'VERTU', key: 'vertu', quantity: 13},
        {title: 'VERYKOOL', key: 'verykool', quantity: 24},
        {title: 'VIVO', key: 'vivo', quantity: 16},
        {title: 'VK MOBILE', key: 'vk mobile', quantity: 26},
        {title: 'VODAFONE', key: 'vodafone', quantity: 13},
        {title: 'WIKO', key: 'wiko', quantity: 9},
        {title: 'WND', key: 'wnd', quantity: 25},
        {title: 'XCUTE', key: 'xcute', quantity: 17},
        {title: 'XIAOMI', key: 'xiaomi', quantity: 4},
        {title: 'XOLO', key: 'xolo', quantity: 26},
        {title: 'YEZZ', key: 'yezz', quantity: 8},
        {title: 'YOTA', key: 'yota', quantity: 8},
        {title: 'YU', key: 'yu', quantity: 6},
        {title: 'ZTE', key: 'zte', quantity: 20},
      ],
    },
    {
      type: FilterTypes.GROUP_CHECKBOX,
      title: 'Material',
      key: 'material',
      values: [
        {title: 'Leather', key: 'leather', quantity: 60},
        {title: 'Wood', key: 'wood', quantity: 19},
        {title: 'Fabric', key: 'fabric', quantity: 31},
        {title: 'Polyester', key: 'polyester', quantity: 31},
      ],
    },
    {
      type: FilterTypes.HORIZONTAL_BUTTON_CHECKBOX,
      title: 'Size',
      key: 'size',
      values: [
        {title: '41', key: '41'},
        {title: '44', key: '44'},
        {title: '42', key: '42'},
        {title: 'XS', key: 'xs'},
        {title: 'M', key: 'm'},
        {title: 'XXL', key: 'xxl'},
      ],
    },
    {
      type: FilterTypes.CHECKBOX,
      title: 'Products with warranty',
      key: 'productsWithWarranty',
    },
    {
      type: FilterTypes.CHECKBOX,
      title: 'Posts with discounts',
      key: 'postsWithDiscounts',
    },
  ],

  items: (() => {
    /*
    Item structure
    {
      title: 'Item 1',
      properties: [
        {
          title: 'Buying Format',
          key: 'buyingFormat',
          value: {title: 'Auction', key: 'auction'},
        },
        {
          title: 'Condition',
          key: 'condition',
          value: {title: 'New', key: 'new'},
        },
        {
          title: 'Category',
          key: 'category',
          value: {title: 'Smart Phones', key: 'smartPhones'},
        },
        {
          title: 'Price',
          key: 'price',
          value: 50,
        },
        {
          title: 'Quantity',
          key: 'quantity',
          value: 30,
        },
        {
          title: 'Seller rate',
          key: 'sellerRate',
          value: 3.5,
        },
        {
          title: 'Brand',
          key: 'brand',
          value: {title: 'APPLE', key: 'apple', quantity: 2},
        },
        {
          title: 'Material',
          key: 'material',
          value: {title: 'Leather', key: 'leather'},
        },
        {
          title: 'Size',
          key: 'size',
          value: {title: '44', key: '44'},
        },
        {
          title: 'Products with warranty',
          key: 'productsWithWarranty',
          value: true,
        },
        {
          title: 'Posts with discounts',
          key: 'postsWithDiscounts',
          value: false,
        },
      ],
    },
*/
    brands = [
      'ACER',
      'ALCATEL',
      'ALLVIEW',
      'AMAZON',
      'AMOI',
      'APPLE',
      'ARCHOS',
      'ASUS',
      'AT & T',
      'BENEFON',
      'BENQ',
      'BENQ-SIEMENS',
      'BIRD',
      'BLACKBERRY',
      'BLACKVIEW',
      'BLU',
      'BOSCH',
      'BQ',
      'CASIO',
      'CAT',
      'CELKON',
      'CHEA',
      'COOLPAD',
      'DELL',
      'EMPORIA',
      'ENERGIZER',
      'ERICSSON',
      'ETEN',
      'FAIRPHONE',
      'FUJITSU',
      'SIEMENS',
      'GARMIN-ASUS',
      'GIGABYTE',
      'GIONEE',
      'GOOGLE',
      'HAIER',
      'HONOR',
      'HP',
      'HTC',
      'HUAWEI',
      'I-MATE',
      'I-MOBILE',
      'ICEMOBILE',
      'INFINIX',
      'INNOSTREAM',
      'INQ',
      'INTEX',
      'JOLLA',
      'KARBONN',
      'KYOCERA',
      'LAVA',
      'LEECO',
      'LENOVO',
      'LG',
      'MAXON',
      'MAXWEST',
      'MEIZU',
      'MICROMAX',
      'MICROSOFT',
      'MITAC',
      'MITSUBISHI',
      'MODU',
      'MOTOROLA',
      'MWG',
      'NEC',
      'NEONODE',
      'NIU',
      'NOKIA',
      'NVIDIA',
      'O2',
      'ONEPLUS',
      'OPPO',
      'ORANGE',
      'PALM',
      'PANASONIC',
      'PANTECH',
      'PARLA',
      'PHILIPS',
      'PLUM',
      'POSH',
      'PRESTIGIO',
      'QMOBILE',
      'QTEK',
      'RAZER',
      'REALME',
      'SAGEM',
      'SAMSUNG',
      'SENDO',
      'SEWON',
      'SHARP',
      'SIEMENS',
      'SONIM',
      'SONY',
      'SONY',
      'ERICSSON',
      'SPICE',
      'T-MOBILE',
      'TCL',
      'TECNO',
      'TEL.ME.',
      'TELIT',
      'THURAYA',
      'TOSHIBA',
      'ULEFONE',
      'UNNECTO',
      'VERTU',
      'VERYKOOL',
      'VIVO',
      'VK MOBILE',
      'VODAFONE',
      'WIKO',
      'WND',
      'XCUTE',
      'XIAOMI',
      'XOLO',
      'YEZZ',
      'YOTA',
      'YU',
      'ZTE',
    ];

    let items = [];

    let itemsCount = 50 + parseInt((Math.random() * 1000) % 50);
    for (let index = 0; index < itemsCount; index++) {
      let buyingFormatIndex = parseInt((Math.random() * 1000) % 2);
      let conditionIndex = parseInt((Math.random() * 1000) % 2);
      let categoryIndex = parseInt((Math.random() * 1000) % 2);
      let priceIndex = parseInt((Math.random() * 10000) % 5000);
      let quantityIndex = parseInt((Math.random() * 1000) % 90);
      let sellerRateIndex =
        parseInt((Math.random() * 1000) % 5) +
        parseInt((Math.random() * 1000) % 9) / 10;
      let brandIndex = parseInt((Math.random() * 1000) % 119);
      let brandQuantityIndex = parseInt((Math.random() * 1000) % 30);
      let materialIndex = parseInt((Math.random() * 1000) % 4);
      let sizeIndex = parseInt((Math.random() * 1000) % 6);
      let productsWithWarrantyIndex = parseInt((Math.random() * 1000) % 2);
      let postsWithDiscountsIndex = parseInt((Math.random() * 1000) % 2);

      let buyingFormatTitle = '';
      let buyingFormatValue = '';
      switch (buyingFormatIndex) {
        case 0:
          buyingFormatTitle = 'Auction';
          buyingFormatValue = 'auction';
          break;
        case 1:
          buyingFormatTitle = 'Direct Sale';
          buyingFormatValue = 'directSale';
          break;
      }

      let conditionTitle = '';
      let conditionValue = '';
      switch (conditionIndex) {
        case 0:
          conditionTitle = 'New';
          conditionValue = 'new';
          break;
        case 1:
          conditionTitle = 'Used';
          conditionValue = 'used';
          break;
      }

      let categoryTitle = '';
      let categoryValue = '';
      switch (categoryIndex) {
        case 0:
          categoryTitle = 'Smart Phones';
          categoryValue = 'smartPhones';
          break;
        case 1:
          categoryTitle = 'Laptops';
          categoryValue = 'laptops';
          break;
      }

      let materialTitle = '';
      let materialValue = '';
      switch (materialIndex) {
        case 0:
          materialTitle = 'Leather';
          materialValue = 'leather';
          break;
        case 1:
          materialTitle = 'Wood';
          materialValue = 'wood';
          break;
        case 2:
          materialTitle = 'Fabric';
          materialValue = 'fabric';
          break;
        case 3:
          materialTitle = 'Polyester';
          materialValue = 'polyester';
          break;
      }

      let sizeTitle = '';
      let sizeValue = '';
      switch (sizeIndex) {
        case 0:
          sizeTitle = '41';
          sizeValue = '41';
          break;
        case 1:
          sizeTitle = '44';
          sizeValue = '44';
          break;
        case 2:
          sizeTitle = '42';
          sizeValue = '42';
          break;
        case 3:
          sizeTitle = 'XS';
          sizeValue = 'xs';
          break;
        case 4:
          sizeTitle = 'M';
          sizeValue = 'm';
          break;
        case 5:
          sizeTitle = 'XXL';
          sizeValue = 'xxl';
          break;
      }

      items.push({
        title: `Item ${index + 1}`,
        properties: [
          {
            title: 'Buying Format',
            key: 'buyingFormat',
            value: {title: buyingFormatTitle, key: buyingFormatValue},
          },
          {
            title: 'Condition',
            key: 'condition',
            value: {title: conditionTitle, key: conditionValue},
          },
          {
            title: 'Category',
            key: 'category',
            value: {title: categoryTitle, key: categoryValue},
          },
          {
            title: 'Price',
            key: 'price',
            value: priceIndex,
          },
          {
            title: 'Quantity',
            key: 'quantity',
            value: quantityIndex,
          },
          {
            title: 'Seller rate',
            key: 'sellerRate',
            value: sellerRateIndex,
          },
          {
            title: 'Brand',
            key: 'brand',
            value: {
              title: brands[brandIndex],
              key: brands[brandIndex].toLowerCase(),
              quantity: brandQuantityIndex,
            },
          },
          {
            title: 'Material',
            key: 'material',
            value: {title: materialTitle, key: materialValue},
          },
          {
            title: 'Size',
            key: 'size',
            value: {title: sizeTitle, key: sizeValue},
          },
          {
            title: 'Products with warranty',
            key: 'productsWithWarranty',
            value: productsWithWarrantyIndex ? true : false,
          },
          {
            title: 'Posts with discounts',
            key: 'postsWithDiscounts',
            value: postsWithDiscountsIndex ? true : false,
          },
        ],
      });
    }

    return items;
  })(),
};

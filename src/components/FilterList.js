import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Accordion from 'react-native-collapsible/Accordion';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import Down from '../assets/down.svg';
import Upw from '../assets/upw.svg';
import FilterTypes from '../Utils/FilterTypes';
import CheckBox from './CheckBox';
import CheckButton from './CheckButton';
import MaxSeek from './MaxSeek';
import MinMaxSeek from './MinMaxSeek';
import SwitchButton from './SwitchButton';
var _ = require('lodash');

const FilterList = props => {
  const _renderHeader = (section, index, isActive) => {
    return (
      <View
        key={index}
        style={{
          borderTopWidth: index > 0 ? 1 : 0,
          ...styles.headerContainer,
        }}>
        <Text
          style={{
            fontSize: 20,
            color: 'white',
            textAlign: 'left',
          }}>{`${section.title}`}</Text>
        {isActive ? (
          <Upw width={20} height={20} />
        ) : (
          <Down width={20} height={20} />
        )}
      </View>
    );
  };
  const _renderContent = (section, index, isActive) => {
    switch (section.type) {
      case FilterTypes.HORIZONTAL_BUTTON_RADIO:
        return (
          <ScrollView
            key={index}
            style={{
              marginBottom: 16,
              marginLeft: 32,
              marginRight: 32,
            }}
            horizontal={true}>
            {section.values.map((value, i) => {
              return (
                <CheckButton
                  key={i}
                  onPressed={value => {
                    props.updateSelection({
                      index: index,
                      update: {
                        selectedItemKey:
                          section.selectedItemKey !== value ? value : null,
                      },
                    });
                  }}
                  style={{marginRight: 16}}
                  item={value}
                  isActive={
                    section.selectedItemKey === value.key
                  }></CheckButton>
              );
            })}
          </ScrollView>
        );
      case FilterTypes.GROUP_CHECKBOX:
        return (
          <View
            key={index}
            style={{
              marginBottom: 16,
              marginLeft: 32,
              marginRight: 32,
            }}>
            {section?.values
              ?.slice(
                0,
                !section.viewMore
                  ? Math.min(section?.values?.length, 8)
                  : section?.values?.length,
              )
              .map((value, i) => {
                return (
                  <CheckBox
                    key={i}
                    onPressed={isChecked => {
                      let mSelectedItemsKeys = [
                        ...(section.selectedItemsKeys || []),
                      ];
                      if (isChecked) {
                        mSelectedItemsKeys.push(value.key);
                      } else {
                        mSelectedItemsKeys.splice(
                          mSelectedItemsKeys.indexOf(value.key),
                          1,
                        );
                      }
                      props.updateSelection({
                        index: index,
                        update: {selectedItemsKeys: mSelectedItemsKeys},
                      });
                    }}
                    style={{alignSelf: 'flex-start', marginLeft: 16}}
                    item={value}
                    isActive={
                      section.selectedItemsKeys?.indexOf(value.key) >= 0
                    }></CheckBox>
                );
              })}
            {section.viewMore != true && section?.values?.length > 8 && (
              <TouchableOpacity
                onPress={() => {
                  props.updateSelection({
                    index: index,
                    update: {viewMore: true},
                  });
                }}
                style={{marginLeft: 20, marginTop: 8}}>
                <Text
                  style={{
                    color: 'white',

                    fontWeight: '600',
                    textDecorationLine: 'underline',
                  }}>{`View all (${section?.values?.length})`}</Text>
              </TouchableOpacity>
            )}
          </View>
        );
      case FilterTypes.HORIZONTAL_BUTTON_CHECKBOX:
        return (
          <ScrollView
            key={index}
            style={{
              marginBottom: 16,
              marginLeft: 32,
              marginRight: 32,
            }}
            horizontal={true}>
            {section?.values?.map((value, i) => {
              return (
                <CheckButton
                  key={i}
                  onPressed={itemKey => {
                    let mSelectedItemsKeys = [
                      ...(section.selectedItemsKeys || []),
                    ];
                    if (!(section.selectedItemsKeys?.indexOf(value.key) >= 0)) {
                      mSelectedItemsKeys.push(value.key);
                    } else {
                      mSelectedItemsKeys.splice(
                        mSelectedItemsKeys.indexOf(value.key),
                        1,
                      );
                    }
                    props.updateSelection({
                      index: index,
                      update: {selectedItemsKeys: mSelectedItemsKeys},
                    });
                  }}
                  style={{marginRight: 16}}
                  item={value}
                  isActive={
                    section.selectedItemsKeys?.indexOf(value.key) >= 0
                  }></CheckButton>
              );
            })}
          </ScrollView>
        );

      case FilterTypes.CHECKBOX:
        return (
          <SwitchButton
            key={index}
            onToggle={toggle => {
              props.updateSelection({
                index: index,
                update: {selectedValue: toggle},
              });
            }}
            style={{marginRight: 16}}
            item={section}
            isActive={!!section.selectedValue}></SwitchButton>
        );

      case FilterTypes.MIN_MAX_SEEK:
        return (
          <View
            key={index}
            style={{marginLeft: 32, marginRight: 32, height: 110}}>
            <MinMaxSeek
              min={section.min}
              max={section.max}
              selectedMin={section.selectedMin || section.min}
              selectedMax={section.selectedMax || section.max}
              onValuesChangeFinish={values => {
                props.updateSelection({
                  index: index,
                  update: {selectedMin: values[0], selectedMax: values[1]},
                });
              }}
              onValuesChange={values => {
                props.updateSelection({
                  index: index,
                  update: {selectedMin: values[0], selectedMax: values[1]},
                  noFetch: true,
                });
              }}></MinMaxSeek>
          </View>
        );

      case FilterTypes.SEEK:
        return (
          <View
            key={index}
            style={{marginLeft: 32, marginRight: 32, height: 60}}>
            <MaxSeek
              step={0.5}
              min={section.min}
              max={section.max}
              selectedValue={section.selectedValue || section.min}
              onValuesChangeFinish={values => {
                props.updateSelection({
                  index: index,
                  update: {selectedValue: values[0]},
                });
              }}
              onValuesChange={values => {
                props.updateSelection({
                  index: index,
                  update: {selectedValue: values[0]},
                  noFetch: true,
                });
              }}></MaxSeek>
          </View>
        );
    }
  };

  const _updateSections = activeSections => {
    !!props.onChange && props.onChange(activeSections);
    _.difference(props.openFilters, activeSections).forEach(sec => {
      props.updateSelection({
        index: sec,
        update: {viewMore: undefined},
      });
    });
  };

  return (
    <Accordion
      disabled={false}
      renderChildrenCollapsed={true}
      renderAsFlatList={true}
      expandMultiple={true}
      activeSections={props.openFilters}
      sections={props.filters}
      renderHeader={_renderHeader}
      renderContent={_renderContent}
      onChange={_updateSections}
      underlayColor={'#00000000'}
    />
  );
};

export default FilterList;

const styles = StyleSheet.create({
  headerContainer: {
    borderTopColor: '#298dbe',
    flexDirection: 'row',
    alignContent: 'space-between',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 32,
    marginRight: 32,
    paddingTop: 20,
    paddingBottom: 20,
  },
  headerText: {color: 'white', textAlign: 'left'},
});

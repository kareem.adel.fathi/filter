import React from 'react';
import {StyleSheet, Text, View, Switch} from 'react-native';

const SwitchButton = props => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{`${props.item.title}`}</Text>
      <Switch
        trackColor={{false: '#056188', true: '#a3cee3'}}
        thumbColor={props.isActive ? 'white' : '#0078b2'}
        ios_backgroundColor="#3e3e3e"
        onValueChange={props.onToggle}
        value={props.isActive}
      />
    </View>
  );
};

export default SwitchButton;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignContent: 'space-between',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 32,
    marginRight: 32,
    paddingBottom: 20,
  },
  title: {
    fontSize: 20,
    color: 'white',
    textAlign: 'left',
  },
});

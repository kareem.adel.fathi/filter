import React from 'react';
import {StyleSheet, Text, useWindowDimensions, View} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
const MinMaxSeek = props => {
  const {width, height} = useWindowDimensions();
  return (
    <>
      <MultiSlider
        values={[props.selectedMin, props.selectedMax]}
        sliderLength={width - 64}
        min={props.min}
        max={props.max}
        step={1}
        allowOverlap={false}
        minMarkerOverlapDistance={40}
        onValuesChangeFinish={props.onValuesChangeFinish}
        onValuesChange={props.onValuesChange}
        customMarker={e => {
          return <View style={styles.marker}></View>;
        }}
      />
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignContent: 'center',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <View style={styles.selectedValueContainer}>
          <Text style={styles.selectedValueText}>{props.selectedMin}</Text>
        </View>
        <View style={styles.selectedValueContainer}>
          <Text style={styles.selectedValueText}>{props.selectedMax}</Text>
        </View>
      </View>
    </>
  );
};

export default MinMaxSeek;

const styles = StyleSheet.create({
  marker: {
    overflow:"hidden",
    backgroundColor: '#0078b2',
    borderColor: 'white',
    borderRadius: 10,
    borderWidth: 2,
    width: 20,
    height: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  selectedValueContainer: {
    padding: 8,
    backgroundColor: '#0078b2',
    borderWidth: 1,
    borderColor: '#298dbe',
    flexDirection: 'row',
    borderRadius: 10,
    justifyContent: 'center',
  },
  selectedValueText: {color: 'white', fontWeight: '500'},
});

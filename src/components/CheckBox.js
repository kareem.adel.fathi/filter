import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Checkb from '../assets/checkb.svg';

const CheckBox = props => {
  return (
    <TouchableOpacity
      onPress={() => {
        !!props.onPressed && props.onPressed(!props.isActive);
      }}
      style={{
        ...styles.checkBoxContainer,
        ...props.style,
      }}>
      <View
        style={{
          backgroundColor: props.isActive ? 'white' : '#00000000',
          borderRadius: 5,
          padding: 2,
          borderColor: props.isActive ? 'white' : '#52a3ca',
          borderWidth: 1,
        }}>
        {
          <Checkb
            style={{opacity: props.isActive ? 1 : 0}}
            width={15}
            height={15}
          />
        }
      </View>
      <Text style={styles.title}>
        {`${props.item.title}${
          !!props.item.quantity ? ` (${props.item.quantity})` : ''
        }`}
      </Text>
    </TouchableOpacity>
  );
};

export default CheckBox;

const styles = StyleSheet.create({
  checkBoxContainer: {
    padding: 8,
    flexDirection: 'row',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {color: 'white', marginLeft: 8, fontWeight: '500'},
});

import React from 'react';
import {StyleSheet, Text, useWindowDimensions, View} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
const MaxSeek = props => {
  const {width, height} = useWindowDimensions();
  return (
    <View style={styles.container}>
      <MultiSlider
        values={[props.selectedValue]}
        sliderLength={width - 64 - 50}
        min={props.min}
        max={props.max}
        step={props.step || 1}
        snapped={true}
        onValuesChangeFinish={props.onValuesChangeFinish}
        onValuesChange={props.onValuesChange}
        customMarker={e => {
          return <View style={styles.marker}></View>;
        }}
      />
      <View style={styles.selectedValue}>
        <Text style={{color: 'white', fontWeight: '500'}}>
          {props.selectedValue}
        </Text>
      </View>
    </View>
  );
};

export default MaxSeek;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  marker: {
    backgroundColor: '#0078b2',
    borderColor: 'white',
    borderRadius: 10,
    borderWidth: 2,
    width: 20,
    height: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  selectedValue: {
    padding: 8,
    backgroundColor: '#0078b2',
    borderWidth: 1,
    borderColor: '#298dbe',
    flexDirection: 'row',
    borderRadius: 10,
    justifyContent: 'center',
  },
});

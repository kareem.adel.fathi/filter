import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Check from '../assets/check.svg';

const CheckButton = props => {
  return (
    <TouchableOpacity
      onPress={() => {
        !!props.onPressed && props.onPressed(props.item.key);
      }}
      style={{
        backgroundColor: props.isActive ? '#298dbe' : '#0078b2',
        ...styles.container,
        ...props.style,
      }}>
      {!!props.isActive && (
        <Check style={{marginRight: 8}} width={20} height={20} />
      )}
      <Text style={styles.title}>{props.item.title}</Text>
    </TouchableOpacity>
  );
};

export default CheckButton;

const styles = StyleSheet.create({
  container: {
    padding: 8,
    borderWidth: 1,
    borderColor: '#298dbe',
    flexDirection: 'row',
    borderRadius: 10,
    justifyContent: 'center',
  },
  title: {color: 'white', fontWeight: '500'},
});

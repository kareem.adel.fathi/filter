var _ = require('lodash');

export const getItems = async ({state, effects, actions}) => {
  const ret = await effects.api.getItems(state.filters);
  state.items = ret.items;
  state.filters = ret.filters.map(filter => {
    filterInStateRet =
      state.filters.find(filterInState => {
        return filterInState.key === filter.key;
      }) || {};
    return _.merge(filterInStateRet, filter);
  });
};

export const setOpenFilters = async (
  {state, effects, actions},
  openFilters,
) => {
  state.openFilters = openFilters;
};

export const updateSelection = async (
  {state, effects, actions},
  {index, update, noFetch},
) => {
  Object.keys(update || {}).forEach(key => {
    state.filters[index][key] = update[key];
  });
  !noFetch && actions.getItems();
};

export const resetFilters = async ({state, effects, actions}) => {
  state.openFilters = [];
  state.filters.forEach((item, index) => {
    switch (item?.type) {
      case FilterTypes.HORIZONTAL_BUTTON_RADIO:
        item.selectedItemKey = undefined;
        break;
      case FilterTypes.GROUP_CHECKBOX:
        item.selectedItemsKeys = undefined;
        break;
      case FilterTypes.MIN_MAX_SEEK:
        item.selectedMin = undefined;
        item.selectedMax = undefined;
        break;
      case FilterTypes.SEEK:
        item.selectedValue = undefined;
        break;
      case FilterTypes.HORIZONTAL_BUTTON_CHECKBOX:
        item.selectedItemsKeys = undefined;
        break;
      case FilterTypes.CHECKBOX:
        item.selectedValue = undefined;
        break;
    }
  });

  actions.getItems();
};

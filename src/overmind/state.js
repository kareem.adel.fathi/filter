import {derived} from 'overmind';

export const state = {
  filters: [],
  openFilters: [],
  items: [],
  itemsCount: derived((state, rootState) => {
    return state.items.length;
  }),
};

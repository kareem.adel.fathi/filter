import {mockItemsWithFilters} from '../../Utils/Globals';

export default {
  getItems: async appliedFilters => {
    //mock api
    let itemsWithFilters = {...mockItemsWithFilters};

    if (appliedFilters === []) {
      return itemsWithFilters;
    }

    return {
      items: itemsWithFilters.items.filter(item => {
        ret = true;
        for (let index = 0; index < item.properties.length; index++) {
          const property = item.properties[index];
          const appliedFilter = appliedFilters.find(appliedFilter => {
            return appliedFilter.key === property.key;
          });
          switch (appliedFilter?.type) {
            case FilterTypes.HORIZONTAL_BUTTON_RADIO:
              ret =
                appliedFilter.selectedItemKey === null ||
                appliedFilter.selectedItemKey === undefined ||
                appliedFilter.selectedItemKey === property.value.key;
              break;
            case FilterTypes.GROUP_CHECKBOX:
              ret =
                appliedFilter.selectedItemsKeys === null ||
                appliedFilter.selectedItemsKeys === undefined ||
                appliedFilter.selectedItemsKeys?.length === 0 ||
                appliedFilter.selectedItemsKeys?.indexOf(property.value.key) >=
                  0;
              break;
            case FilterTypes.MIN_MAX_SEEK:
              ret =
                appliedFilter.selectedMin === null ||
                appliedFilter.selectedMin === undefined ||
                appliedFilter.selectedMax === null ||
                appliedFilter.selectedMax === undefined ||
                (appliedFilter.selectedMin <= property.value &&
                  appliedFilter.selectedMax >= property.value);
              break;
            case FilterTypes.SEEK:
              ret =
                appliedFilter.selectedValue === null ||
                appliedFilter.selectedValue === undefined ||
                appliedFilter.selectedValue <= property.value;
              break;
            case FilterTypes.HORIZONTAL_BUTTON_CHECKBOX:
              ret =
                appliedFilter.selectedItemsKeys === null ||
                appliedFilter.selectedItemsKeys === undefined ||
                appliedFilter.selectedItemsKeys?.length === 0 ||
                appliedFilter.selectedItemsKeys?.indexOf(property.value.key) >=
                  0;
              break;
            case FilterTypes.CHECKBOX:
              ret =
                appliedFilter.selectedValue === null ||
                appliedFilter.selectedValue === undefined ||
                appliedFilter.selectedValue === false ||
                appliedFilter.selectedValue === property.value;
              break;
          }
          if (ret === false) {
            break;
          }
        }
        return ret;
      }),
      filters: itemsWithFilters.filters,
    };
  },
};

import React, {useCallback, useEffect, useMemo, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  useWindowDimensions,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {useActions, useState} from '../overmind';
import {useNavigation} from '@react-navigation/native';
import FilterList from '../components/FilterList';
import Up from '../assets/up.svg';
//import Check from "../assets/check.svg";
//import X from "../assets/x.svg";
//import Up from "../assets/up.svg";
//<Check width={40} height={40} fill={"any color"} />

const Home = () => {
  const {width, height} = useWindowDimensions();
  const actions = useActions();
  const state = useState();
  const navigation = useNavigation();

  useEffect(() => {
    actions.getItems();
    return () => {};
  }, []);
  return (
    <View style={styles.container}>
      <View style={styles.filterListContainer}>
        <FilterList
          updateSelection={actions.updateSelection}
          filters={state.filters}
          onChange={items => {
            actions.setOpenFilters(items);
          }}
          openFilters={state.openFilters}></FilterList>
      </View>
      <TouchableOpacity
        onPress={() => {
          Alert.alert(
            'Selected items',
            state.items.reduce((total, item) => {
              total += item.title + '\n';
              return total;
            }, ''),
            [{text: 'OK'}],
          );
        }}
        style={styles.bottomButton}>
        <Text style={{fontWeight: '500'}}>{`${state.itemsCount} Results`}</Text>
        <Up width={20} height={20} />
      </TouchableOpacity>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {flex: 1},
  filterListContainer: {flex: 1, paddingTop: 16},
  bottomButton: {
    width: '100%',
    height: 60,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'space-between',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 32,
    paddingRight: 32,
  },
});

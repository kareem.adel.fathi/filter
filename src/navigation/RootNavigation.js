import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {DefaultTheme, NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {useActions, useState} from '../overmind';
import Home from '../screens/Home';
import X from '../assets/x.svg';
//<X width={40} height={40} fill={"any color"} />

const Stack = createStackNavigator();

const RootNavigation = () => {
  const actions = useActions();
  const state = useState();

  return (
    <NavigationContainer
      theme={{
        ...DefaultTheme,
        colors: {
          ...DefaultTheme.colors,
          background: '#0078b2',
        },
      }}>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={Home}
          options={{
            title: 'Filter Results',
            headerLeft: () => (
              <TouchableOpacity
                style={styles.reset}
                onPress={() => {
                  actions.resetFilters();
                }}>
                <X width={25} height={25} fill={'white'} />
              </TouchableOpacity>
            ),
            headerTitleAlign: 'left',
            headerStyle: styles.headerStyle,
            headerTitleStyle: {color: 'white'},
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigation;

const styles = StyleSheet.create({
  reset: {marginLeft: 16},
  headerStyle: {
    backgroundColor: '#0078b2',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
});

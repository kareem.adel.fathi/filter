/* eslint-disable react/jsx-no-undef */
import 'react-native-gesture-handler';
import React from 'react';
import {Node} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  StatusBar,
} from 'react-native';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import RootNavigation from './src/navigation/RootNavigation';
import {createOvermind} from 'overmind';
import {config} from './src/overmind';
import {Provider} from 'overmind-react';
import {DefaultTheme} from '@react-navigation/native';

const overmind = createOvermind(config);

const App = () => {
  return (
    <Provider value={overmind}>
      {/*<StatusBar
        barStyle={'dark-content'}
        backgroundColor={DefaultTheme.colors.background}
      />*/}
      <GestureHandlerRootView style={{flex: 1}}>
        <RootNavigation></RootNavigation>
      </GestureHandlerRootView>
    </Provider>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
